import Link from "next/link"
import Head from "next/head"
import Script from "next/script"

export default function Firstpost() {
  return (
    <>
      <Head>
        <title>firstpost title</title>

        {/*TODO why console.log below not visible?  guided code fr https://nextjs.org/learn/basics/assets-metadata-css/third-party-javascript */}
        <Script src="https://connect.facebook.net/en_US/sdk.js"                                    strategy="lazyOnload" onLoad={() => console.log(`script loaded correctly, window.FB has been populated`) } />

        {/* bootstrap 4 js  ref https://getbootstrap.com/docs/4.6/getting-started/introduction/ */}
        <Script src='https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js'            strategy='afterInteractive' onLoad={() => {console.log('jquery @ js loaded by nextjs Script ')} }/>
        <Script src='https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js' strategy='afterInteractive' onLoad={() => {console.log('bs4 js @ js loaded by nextjs Script ')} }/>
        {/*                                                                                        strategy= ref https://nextjs.org/docs/pages/building-your-application/optimizing/scripts#strategy*/}
      </Head>

      <h3>First Post</h3>
      <h3><Link href='/'>back to home</Link> </h3>
    </>
  )
}
