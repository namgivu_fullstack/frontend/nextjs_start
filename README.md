--- code start 
gg nextjs ~quickstart
ref https://nextjs.org/docs/getting-started

--- server componet
ref https://nextjs.org/docs/getting-started/react-essentials#server-components

> majority of components are non-interactive and can be rendered on the server as Server Components
> [](https://nextjs.org/_next/image?url=%2Fdocs%2Fdark%2Fthinking-in-server-components.png&w=1920&q=75&dpl=dpl_CdUSnANkEdZkEcBVH1CsQtsGxqSh)


== why server component?
> you can move data fetching to the server, closer to your database, 
> keep large dependencies, that previously impact the client bundle size, on the server to render faster in webbrowser
> Server Component make reactjs coding feel similar to PHP or Ruby on Rails, but with the power of React for templating UI


== pattern usage 
ref https://nextjs.org/docs/getting-started/react-essentials#recommended-pattern-passing-server-components-to-client-components-as-props

> Passing Server Components to Client Components as Props
> Server Component will be rendered on the server, and when the Client Component is rendered on the client, the "slot" will be filled in with the rendered result of the Server Component.


== fetch data cached
ref https://nextjs.org/docs/getting-started/react-essentials#sharing-fetch-requests-between-server-components
> fetch requests are automatically memoized in Server Components
> can request exactly the data it needs without worrying about duplicate requests. Next.js will read the same value from the fetch cache.

TODO how to invalidate the cached value aka force-refetch?


--- more ~tutorial ~sample nextjs_app aka nextjs-learn aka nextlearn
gg nextjs ~tutorial ~sample
ref https://nextjs.org/learn/basics/create-nextjs-app

--- nextlearn @ learn-starter
> npx create-next-app@latest nextjsapp230810--nextjs-blog --use-npm --example "https://github.com/vercel/next-learn/tree/master/basics/learn-starter"
>                            nextjsapp230810--nextjs-blog

== run it 
> cd .../nextjsapp230810--nextjs-blog
> npm i ; npm run dev
> browse web at localhost:3000

== multiple pages w/ file-system routing 
ref https://nextjs.org/learn/basics/navigate-between-pages/pages-in-nextjs

== link from homepage to a sub page in pages/
> When linking between pages on websites, you use the <a> HTML tag

> nextjs's <Link> allows you to do client-side navigation 
> and accepts `props` that give you better control over the `navigation behavior`

== client-side navigation?  ref https://nextjs.org/learn/basics/navigate-between-pages/client-side
means that the page transition happens using JavaScript, which is faster than <a href="…"> behavior
ie If you’ve used <a href="…"> instead of <Link href="…"> browser will do a full refresh

== code splitting?  ref https://nextjs.org/learn/basics/navigate-between-pages/client-side
> Next.js does code splitting `automatically`, so each page only loads what’s necessary for that page. 
> That means when the homepage is rendered, the code for other pages is not served initially.
 
> homepage loads quickly even if you have hundreds of pages.

> If a certain page throws an error, the rest would still work.

> need to link to an external page outside the Next.js app, just use an <a> tag without Link.

== prefetched?
> Next.js automatically prefetches the code for the linked page in the background. 
> By the time you click the link, the code for the destination page will already be loaded in the background

== public asset @ image
> use original <img src=... /> means you have to manually handle:
> + ensure responsive
> + optimizing your images
> + only loading images when they enter the viewport
> Next.js provides an <Image/> component out of the box to handle this

> resizing, optimizing, and serving images in `modern formats like WebP` when the browser supports it
> avoids shipping large images to devices with a smaller viewport
> also works with any image source eg external data source like a CMS

> Next.js optimizes images on-demand, as users request them
> your build times aren't increased, whether shipping 10 images or 10 million images.

> Images are lazy loaded by default. 
> page isn't penalized for images outside the viewport ie loaded only when being scrolled into viewport.

> Images are optimized dynamically upon request and stored in the `<distDir>/cache/images` directory
> ref https://nextjs.org/docs/pages/api-reference/components/image#caching-behavior

== public asset @ js
> in <Head /> we can add original <script> block 
> but including scripts in this manner does not give a clear idea 
> of `when it would load` with respect to other JavaScript code in same page

> use nextjs <Script /> instead
> prop `strategy=lazyOnload` tells Next.js to load this particular script lazily during browser idle time
> prop `onLoad`              is used to run js code after the script of <Script /> finish loading
    

--- send POST req @ nextjs internal api endpoint
ref https://stackoverflow.com/a/66740097/248616
> don't need to worry about `cross-browser compatibility` for `fetch` with Next.js. Next.js automatically adds a polyfill when needed

--- nextlearn @ api-routes-starter
> npx create-next-app@latest nextjsapp230822--api-routes-starter --use-npm --example "https://github.com/vercel/next-learn/tree/master/basics/api-routes-starter"
